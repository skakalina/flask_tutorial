import os

from flask import Flask


def create_app(auth=None):
    # создание и настройка приложения
    app = Flask(__name__, instance_relative_config=True) #создает экземпляр Flask
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'flaskr.sqlite'), #обрезает путь к папке
    )

    # убедитесь, что папка c базой существует
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    from . import db
    print('тута')
    db.init_app(app)

    from . import auth
    app.register_blueprint(auth.bp)

    from . import blog
    app.register_blueprint(blog.bp)
    app.add_url_rule('/', endpoint='index')

    # простая страница с приветствием
    @app.route('/hello')
    def hello():
        return 'Hello, World!'

    return app